api = 2
core = 7.x
projects[drupal][version] = 7.36

; Patches
projects[drupal][patch][] = "https://drupal.org/files/drupal-default-date-formats-1817748-5.patch"
projects[drupal][patch][] = "https://www.drupal.org/files/contextual-links-render-cache-914382-184.patch"
