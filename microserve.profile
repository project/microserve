<?php
/**
 * @file
 * Enables modules and site configuration for a Microserve site installation.
 */

/**
 * Helper function to get the site name from the url.
 *
 * @return string
 *   The current hostname minus the TLD and www. prefix, e.g. 'example' for a
 *   hostname of 'www.example.com'.
 */
function _microserve_profile_site_name($site_name = NULL) {
  if (!isset($site_name)) {
    $site_name = $_SERVER['SERVER_NAME'];
  }

  // Strip www. prefix;
  if (strpos($site_name, 'www.') === 0) {
    $site_name = substr($site_name, 4);
  }

  list($site_name) = explode('.', $site_name);

  return $site_name;
}

/**
 * Implements hook_form_FORM_ID_alter() for install_settings_form().
 *
 * Allows the profile to alter the site settings form. Pretend to be system
 * module to get this to work as it is too early in bootstrap for the standard
 * hook_form_*_alter() functions to work.
 *  *
 * @see https://drupal.org/node/1153646
 */
function system_form_install_settings_form_alter(&$form, &$form_state) {
  $show_message = FALSE;
  foreach (array('mysql', 'pgsql') as $key) {
    if (isset($form['settings'][$key]['username'])) {
      // Generate a database name and user account (to be created).
      $form['settings'][$key]['database']['#default_value'] = _microserve_profile_site_name();
      $form['settings'][$key]['username']['#default_value'] = _microserve_profile_site_name();
      // Generate a random 16 character password to be used for the database user account.
      $form['settings'][$key]['password']['#type'] = 'textfield';
      $form['settings'][$key]['password']['#default_value'] = user_password(16);

      $show_message = TRUE;
    }
  }

  if ($show_message) {
    // Show a help message about setting up a new database for the site.
    $form['settings']['message'] = array(
      '#weight' => -100,
      '#type' => 'markup',
      '#markup' => '<div class="messages status">' . st('Please fill in the database connection details or create a new database using the following details:') . '</div>',
    );
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function microserve_form_install_configure_form_alter(&$form, $form_state) {
  // Clear the module enable messages that some modules add. These do not apply
  // yet, and could lead the user away from installation.
  drupal_get_messages();
  drupal_set_message(st('Please configure your new site:'));

  // Add a description for the Site name field.
  $form['site_information']['site_name']['#description'] = st('The publicly visible full name of the website or client / company.');

  // Lock-out the uid #1 account.
  $admin_name = 'admin_' . user_password(8);
  $form['admin_account']['account']['name']['#type'] = 'value';
  $form['admin_account']['account']['name']['#value'] = $admin_name;
  $form['admin_account']['account']['mail']['#type'] = 'value';
  $form['admin_account']['account']['mail']['#value'] = $admin_name . '@example.com';
  $form['admin_account']['account']['pass']['#type'] = 'value';
  $form['admin_account']['account']['pass']['#value'] = user_password(16);

  // Create a developer account to use.
  $form['admin_account']['#title'] = st('Your developer account');
  $form['admin_account']['#description'] = st('The admin (uid #1) account will be automatically disabled. Please create a personal developer account to use instead:');
  // New account form.
  $form['admin_account']['developer_account']['#tree'] = TRUE;
  $form['admin_account']['developer_account']['name'] = array(
    '#type' => 'textfield',
    '#title' => st('Your username'),
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#description' => st('Spaces are allowed; punctuation is not allowed except for periods, hyphens, and underscores.'),
    '#required' => TRUE,
    '#weight' => -10,
    '#attributes' => array('class' => array('username')),
  );

  $form['admin_account']['developer_account']['mail'] = array(
    '#type' => 'textfield',
    '#title' => st('Your email address'),
    '#maxlength' => EMAIL_MAX_LENGTH,
    '#required' => TRUE,
    '#weight' => -5,
  );
  $form['admin_account']['developer_account']['pass'] = array(
    '#type' => 'password_confirm',
    '#required' => TRUE,
    '#size' => 25,
    '#weight' => 0,
  );

  // Add a form submission handler to add our developer account.
  $form['#submit'][] = 'microserve_form_install_configure_form_submit';

  // Pre-set the country and timezone settings for UK.
  $form['server_settings']['site_default_country']['#default_value'] = 'GB';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'Europe/London';

  // Turn off update email alerts.
  $form['update_notifications']['update_status_module']['#default_value'] = array(1);
}

/**
 * Form submissions handler for install_configure_form().
 */
function microserve_form_install_configure_form_submit($form, &$form_state) {
  global $user;

  // Create the developer account.
  $merge_data = array(
    'init' => $form_state['values']['developer_account']['mail'],
    'roles' => array(variable_get('user_admin_role', 0) => 'administrator'),
    'status' => 1,
    'timezone' => $form_state['values']['date_default_timezone'],
  );
  $account = user_save(NULL, array_merge($form_state['values']['developer_account'], $merge_data));
  // Load global $user and perform final login tasks.
  $user = user_load($account->uid);
  user_login_finalize();

  // Disable the admin account.
  $admin_account = user_load(1);
  user_save($admin_account, array('status' => 0));
}

/**
 * Implements hook_install_tasks_alter().
 */
function microserve_install_tasks_alter(&$tasks, $install_state) {
  // Replace the "Choose language" installation task provided by Drupal core.
  $tasks['install_select_locale']['function'] = 'microserve_locale_selection';
  $tasks['install_finished']['function'] = 'microserve_install_finished';
}

function microserve_locale_selection(&$install_state) {
  // Set the locale to English.
  $install_state['parameters']['locale'] = 'en';
}

function microserve_install_finished(&$install_state) {
  $message = st('Congratulations, you installed @drupal!', array('@drupal' => drupal_install_profile_distribution_name()));

  // Flush all caches to ensure that any full bootstraps during the installer
  // do not leave stale cached data, and that any content types or other items
  // registered by the installation profile are registered correctly.
  drupal_flush_all_caches();

  // Remember the profile which was used.
  variable_set('install_profile', drupal_get_profile());

  // Installation profiles are always loaded last
  db_update('system')
    ->fields(array('weight' => 1000))
    ->condition('type', 'module')
    ->condition('name', drupal_get_profile())
    ->execute();

  // Cache a fully-built schema.
  drupal_get_schema(NULL, TRUE);

  // Ensure system file directories are setup correctly.
  $public = variable_get('file_public_path', conf_path() . '/files');
  system_check_directory(array('#value' => $public, '#name' => 'file_public_path'));
  system_check_directory(array('#value' => variable_get('file_private_path', $public . '/private'), '#name' => 'file_private_path'));
  system_check_directory(array('#value' => variable_get('file_temporary_path', $public . '/tmp'), '#name' => 'file_temporary_path'));

  if (module_exists('features')) {
    // Revert the text format feature. This resets the CKEditor profiles.
    features_revert_module('microserve_text_formats');
  }

  // Turn on performance settings.
  variable_set('cache', TRUE);
  variable_set('page_cache_maximum_age', 86400);
  variable_set('preprocess_css', TRUE);
  variable_set('preprocess_js', TRUE);

  // Run cron to populate update status tables (if available) so that users
  // will be warned if they've installed an out of date Drupal version.
  // Will also trigger indexing of profile-supplied content or feeds.
  drupal_cron_run();

  // Set a congratulation message and send the user on to their new site.
  drupal_set_message($message);
  $url = '<front>';
  if (module_exists('styleguide')) {
    $url = 'admin/appearance/styleguide';
  }
  drupal_goto($url);
}
