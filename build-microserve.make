api = 2
core = 7.x
includes[] = drupal-org-core.make
projects[microserve][type] = profile
projects[microserve][download][type] = git
projects[microserve][download][url] = http://git.drupal.org/project/microserve.git
projects[microserve][download][branch] = 7.x-2.x
