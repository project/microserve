<?php
/**
 * @file
 * microserve_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function microserve_content_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function microserve_content_types_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Content page'),
      'base' => 'node_content',
      'description' => t('Use <em>content pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
