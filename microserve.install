<?php
/**
 * @file
 * Install, update and uninstall functions for the Microserve installation profile.
 */

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function microserve_install() {
  // Disable user registrations by default.
  variable_set('user_register', USER_REGISTER_ADMINISTRATORS_ONLY);

  // Enable default permissions for system roles.
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access content'));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content'));

  // Create a default role for developers, with all available permissions assigned.
  $admin_role = new stdClass();
  $admin_role->name = 'developer';
  $admin_role->weight = 2;
  user_role_save($admin_role);
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));
  // Set this as the administrator role.
  variable_set('user_admin_role', $admin_role->rid);

  $theme = 'bootstrap';
  $admin_theme = 'adminimal_theme';
  // Set Bootstrap as the default site theme.
  variable_set('theme_default', $theme);
  // Set Adminimal as the site admin theme.
  variable_set('admin_theme', $admin_theme);
  variable_set('node_admin_theme', '1');

  // Enable both themes.
  db_update('system')
    ->fields(array('status' => 1))
    ->condition('type', 'theme')
    ->condition('name', array($theme, $admin_theme))
    ->execute();
  // Disable all other themes.
  db_update('system')
    ->fields(array('status' => 0))
    ->condition('type', 'theme')
    ->condition('name', array($theme, $admin_theme), 'NOT IN')
    ->execute();
}

/**
 * Implements hook_update_N().
 */
function microserve_update_7100(&$sandbox) {
  // Enable the core contextual links module.
  module_enable(array('contextual'));
}
